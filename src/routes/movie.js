import {
    addNewMovie,
    getMovies,
    getMovieWithID,
    updateMovie,
    deleteMovie,
    addRating
} from '../controllers/movie';
import { authenticateJWT, authenticatedAs } from '../utils/auth'

const routes = (app) => {
    /**
     * @swagger
     * /movie:
     *   get:
     *     summary: List movies
     *     consumes:
     *       application/json
     *     responses:
     *       '200':
     *         description:
     *           Movies Listed
     *   post:
     *     summary: List movies
     *     consumes:
     *       application/json
     *     parameters:
     *       - name: movie
     *         in: body
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             name:
     *               type: string
     *             director:
     *               type: string
     *             gender:
     *               type: string
     *             actors:
     *               type: array
     *               items:
     *                 type: string
     *     responses:
     *       '200':
     *         description:
     *           Movies Listed
     *
    */
    app.route('/movie')
        .get(getMovies)
        .post(authenticatedAs('admin'), addNewMovie);

    /**
     * @swagger
     * /movie/{movieID}:
     *   get:
     *     summary: Gets a full information movie
     *     parameters:
     *       - in: path
     *         name: movieID
     *         type: string
     *         required: true
     *         description: ID of the movie to get
     *   put:
     *     summary: Updates movie informations
     *     parameters:
     *       - in: path
     *         name: movieID
     *         type: string
     *         required: true
     *         description: ID of the movie to get
     *       - name: movie
     *         in: body
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             name:
     *               type: string
     *             director:
     *               type: string
     *             gender:
     *               type: string
     *             actors:
     *               type: array
     *               items:
     *                 type: string
     */
    app.route('/movie/:movieID')
        .get(getMovieWithID)
        .put(authenticatedAs('admin'), updateMovie)
        .delete(authenticatedAs('admin'), deleteMovie);

    /**
     * @swagger
     * /movie/{movieID}/rate:
     *   post:
     *     summary: User rates a movie
     *     parameters:
     *       - in: path
     *         name: movieID
     *         type: string
     *         required: true
     *         description: ID of the movie to rate
     *       - name: rate
     *         in: body
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             stars:
     *               type: number
     */
    app.route('/movie/:movieID/rate')
        .post(authenticatedAs('user'), addRating);
}

export default routes;
