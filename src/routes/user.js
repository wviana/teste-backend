import {
    addNewUser,
    updateUser
} from "../controllers/user";
import { authenticatedAs } from '../utils/auth'

const routes = (app) => {
    /**
     * @swagger
     * /user:
     *   post:
     *     summary: Creates a new user
     *     consumes:
     *       application/json
     *     parameters:
     *       - name: user
     *         in: body
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             email:
     *               type: string
     *             password:
     *               type: string
     *             role:
     *               type: string
     *               enum: 
     *                 - user
     *                 - admin
     */
    app.route('/user')
        .post(addNewUser);

    /**
     * @swagger
     * /user/{userID}:
     *   put:
     *     summary: Updates a user information
     *     consumes:
     *       application/json
     *     parameters:
     *       - in: path
     *         name: userID
     *         type: string
     *         required: true
     *         description: ID of the user to update
     *       - name: user
     *         in: body
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             email:
     *               type: string
     *             password:
     *               type: string
     *             role:
     *               type: string
     *               enum: 
     *                 - user
     *                 - admin
     */
    app.route('/user/:userID')
        .put(authenticatedAs('admin'), updateUser);
}

export default routes;
