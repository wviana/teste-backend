import { login } from '../controllers/login'

/**
 * @swagger
 * /login:
 *   post:
 *     summary: User to login with a user
 *     consumes:
 *       application/json
 *     parameters:
 *       - name: login
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       '200':
 *         description:
 *           User successfull login
 *
*/
const routes = (app) => {
    app.route('/login')
        .post(login)
}

export default routes;
