import jwt from 'jsonwebtoken';

const accessTokenSecret = 'mudar1234';

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

const authenticatedAs = (role) => {
    return (req, res, next) => {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, accessTokenSecret, (err, user) => {
                if (err) {
                    return res.sendStatus(403);
                }

                req.user = user;

                if (user.role !== role) {
                    return res.sendStatus(401);
                }
                
                next();
            });
        } else {
            res.sendStatus(401);
        }
    }
};

export { authenticateJWT, authenticatedAs };
