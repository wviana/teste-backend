import mongoose from 'mongoose';
import bcrypt from 'bcrypt'

const SALT_WORK_FACTOR = 11;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        required: true,
        trim: true
    },
    role: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }
}, { timestamps: true });

UserSchema.pre('save', function(next){
    const user = this;

    if (!user.isModified('password')) return next();

    return bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) return next();

        return bcrypt.hash(user.password, salt, (hasherr, hash) => {
            if (hasherr) return next(hasherr);

            user.password = hash;
            return next();
        });
    });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb){
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) return cb(err);
        return cb(null, isMatch);
    })
}

export { UserSchema };
export const User = mongoose.model('User', UserSchema);
