import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const MovieSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    director: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    actors: {
        type: [String],
        required: true
    },
    ratings: {
        type: [{stars: Number, user: mongoose.Types.ObjectId}],
        default: []
    },
}, { timestamps: true });

MovieSchema.virtual('avgRating').get(function(){
    const sum = this.ratings.reduce((a,b) => a += b.stars, 0);
    return sum / this.ratings.length;
});

export { MovieSchema };
export const Movie = mongoose.model('Movie', MovieSchema);
