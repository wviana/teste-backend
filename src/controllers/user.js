import mongoose from 'mongoose';
import { UserSchema } from '../models/user';

const User = mongoose.model('User', UserSchema);

export const addNewUser = (req, res) => {
    let newUser = new User(req.body);

    newUser.save((err, user) => {
        if (err) {
            res.send(err);
        }
        res.json(user)
    });
}

export const updateUser = (req, res) => {
    const { userID } = req.params;

    User.findOne({_id: userID}, (err, user) => {
        if (err) {
            console.log(err)
            return res.send(err);
        }
        Object.assign(user, req.body);
        user.save((err, user) => {
            if (err) {
                console.log(err)
                return res.send(err);
            }
            res.json(user)
        });
    })

}
