import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import { UserSchema } from '../models/user';

const accessTokenSecret = 'mudar1234';

const User = mongoose.model('User', UserSchema);

export const login = (req, res) => {
    const { email, password } = req.body;
    console.log(email, password)
    const errorMessage = 'User or Password incorrect';
    User.findOne({email: email}, (err, user) => {
        if (err) {
            res.send({messsage: errorMessage});
        }
        user.comparePassword(password, (err, isMatch) => {
            if (err || !isMatch) {
                return res.send({messsage: errorMessage});
            }
            const { role } = user;
            const accessToken = jwt.sign({ email, role }, accessTokenSecret);

            res.json(accessToken);
        })
    })
}
