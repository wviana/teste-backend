import { Movie } from '../models/movie'
import { User} from '../models/user'


export const addNewMovie = (req, res) => {
    let newMovie = new Movie(req.body);

    newMovie.save((err, movie) => {
        if (err) {
            res.send(err);
        }
        res.json(movie)
    });
}

export const getMovies = (req, res) => {
    Movie.find(req.query, (err, movies) => {
        if (err) {
            res.send(err);
        }
        res.json(movies)
    });
}

export const getMovieWithID = (req, res) => {
    const id = req.params.movieID;
    Movie.findById(id, (err, movies) => {
        if (err) {
            res.send(err);
        }
        res.send(movies.toJSON({virtuals: true}))
    });
}

export const updateMovie = (req, res) => {
    const { movieID } = req.params;
    Movie.findOneAndUpdate({_id: movieID}, req.body, { new: true, useFindAndModifu: false }, (err, movies) => {
        if (err) {
            res.send(err);
        }
        res.json(movies)
    });
}

export const addRating = (req, res) => {
    const id = req.params.movieID;
    const stars = req.body.stars;

    if ( stars < 0 || stars > 4 ) {
        res.send({message: 'Rating should be between 0 and 4 stars.'})
        return;
    }


    const user = req.user;
    Movie.findById(id, (err, movie) => {
        if (err || !movie) {
            res.send({message: 'Movie not found'});
        }

        User.findOne({email: user.email}, (err, user) => {
            if (err) {
                res.send({message: 'User not found'});
            }
            movie.ratings.push({stars: stars, user})
            movie.save();
            res.json(movie);

        });

    })
}

export const deleteMovie = (req, res) => {
    const id = req.params.movieID;
    Movie.deleteOne({_id: id}, (err, movies) => {
        if (err) {
            res.send(err);
        }
        res.json({message: 'successfully deleted movie'})
    });
}
