# Como rodar esse projeto

- Suba uma instancia do mongodb usando o comando `sudo docker run --name mongo -d -p 27017:27017 mongo`
- Instale as dependências do projeto com `npm install`
- Inicie o projeto com `npm start`
- Existe uma documentação dos endpoints acessando a url `http://localhost:4000/api-docs`, infelizmente os testes por meio do swagger não estão 100% funcionais.
- Existe um export do POSTMAN na raiz deste projeto, nela você encontrará todos os endpoints necessários para testar os requisitos.

# Roteiro de teste

- Selecionar endpoint Add User e adicionar um primeiro usuário de role admin, com o seguinte playload
```json
{
    "email": "root",
    "password": "root",
    "role": "admin"
}
```
- Fazer login com o usuário usando o endpoint Login, passando um payload com email e password. 
- Será retornado um token que pode ser utilizado nas demais requisições.
- Crie um filme no endpoint Create Movie passando o seguinte payload e o token recuperado anteriormente na aba de Authorization do postman
```json
{
    "name": "Back to the future III",
    "director": "Steven Spilberd",
    "gender": "SciFy",
    "actors": ["Michael J Fox"]
}
```
- Listar todos os filmes com endpoint List Movies, que não precisa de autenticação.
- Criar usuário com role "user" 
- Guardar id de filme cadastrado
- Fazer login com usuário do tipo user e usar token em próximo passo.
- Fazer avaliação de filme com Rate Movie, passando id como pathparam e payload com campo stars e um número de 0-4
- Recuperar informações completas de filme com endpoint Get Movie, passando id do filme, nota-se a presença do campo com média de nota.




# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🚨 Requisitos Implementados

- A API foi constuida em **NodeJS**
- Foi implementada autenticação e deverá seguir o padrão **JWT**, lembrando que o token a ser recebido deverá ser no formato **Bearer**
- Como foi desenvolvida em NodeJS o seu projeto foi implementado em **ExpressJS**
- Para a comunicação com o banco de dados foi utilizado o **ODM** mongoose.
- Bancos não relacionais escolhido:
  - MongoDB
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

## Cadastro de admin e usuário unificado em endpoint de cadastro de usuário, mudando somente a role.

- Admin
  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Usuário
  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Filmes
  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro) **OK**
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme) **OK**
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores) **OK**
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos **OK**

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não** **OK**
