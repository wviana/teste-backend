import express from 'express';
import moviesRoutes from './src/routes/movie';
import userRoutes from './src/routes/user';
import loginRoutes from './src/routes/login';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';


const app = express();
const PORT = 4000;

const swaggerOptions = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Wesley's Ioasys Test",
      version: "0.1.0",
      description:
        "This is a simple CRUD API mimics IMDB api.",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "Wesley Viana",
        url: "https://github.com/wviana",
        email: "viana.wesley@gmail.com",
      },
    },
    servers: [
      {
        url: "http://localhost:4000/",
      },
    ],
  },
  apis: ["./src/routes/login.js", "./src/routes/movie.js", "./src/routes/user.js"],
};

const specs = swaggerJsdoc(swaggerOptions);
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs, {explorer: true})
);

// mongoose connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/IMDBdb', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// bodyparser setup
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

moviesRoutes(app);
userRoutes(app);
loginRoutes(app);

app.get('/', (req, res) => res.send(`Node and Express server running on port ${PORT}`));

app.listen(PORT, () => console.log(`Your server is running on port ${PORT}`));
